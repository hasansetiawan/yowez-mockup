/* eslint-env browser */
/* @flow */
import React from 'react';
import Header from './components/Header';

const {Component} = React;

export default class LatestView extends Component {
  render() {
    return (
      <div>
        <Header />
        Latest Page Goes Here!
      </div>
    );
  }
}
