/* eslint-env browser */
/* @flow */

import React from 'react';
import {Link} from 'react-router';

const {Component} = React;

export default class Header extends Component {
  render() {
    return (
      <div>
        <ul role="nav">
          <li><Link to="/home" activeClassName="active">TIMELINE</Link></li>
          <li><Link to="/popular" activeClassName="active">POPULAR</Link></li>
          <li><Link to="/latest" activeClassName="active">LATEST</Link></li>
        </ul>
      </div>
    );
  }
}
