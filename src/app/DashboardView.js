/* eslint-env browser */
/* @flow */
import React from 'react';
import Header from './components/Header';

const {Component} = React;

export default class DashboardView extends Component {
  render() {
    return (
      <div>
        <Header />
        Welcome to dashboard page!
      </div>
    );
  }
}
