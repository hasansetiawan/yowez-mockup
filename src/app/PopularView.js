/* eslint-env browser */
/* @flow */
import React from 'react';
import Header from './components/Header';

const {Component} = React;

export default class PopularView extends Component {
  render() {
    return (
      <div>
        <Header />
        Popular Page Goes Here!
      </div>
    );
  }
}
