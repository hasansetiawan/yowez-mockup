/*eslint-env browser */

import React from 'react';
import ReactDOM from 'react-dom';
import LoginView from './app/LoginView';
import PopularView from './app/PopularView';
import LatestView from './app/LatestView';
import DashboardView from './app/DashboardView';
import {Router, Route, hashHistory} from 'react-router';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Router history={hashHistory} >
      <Route path="/" component={LoginView} />
      <Route path="/home" component={DashboardView} />
      <Route path="/popular" component={PopularView} />
      <Route path="/latest" component={LatestView} />
    </Router>,
    document.querySelector('#app'),
  );

});
